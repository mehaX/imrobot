﻿using ImRobot.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace ImRobot.Persistence
{
    public class ImRobotDbContext : DbContext
    {
        public ImRobotDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Person> People { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
