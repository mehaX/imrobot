﻿namespace ImRobot.Domain.Enumerations
{
    public enum LuisIntent
    {
        None,

        Age,
        Greetings,
        Name,
        Job,
        ChangeUser
    }
}
