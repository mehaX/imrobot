﻿using System;

namespace ImRobot.Domain.Entities
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Birthdate { get; set; }
        public string Job { get; set; }

        public int Age
        {
            get
            {
                return new DateTime(DateTime.Now.Subtract(Birthdate).Ticks).Year - 1;
            }
        }

        public string BirthdateMonth
        {
            get
            {
                return Birthdate.ToString("MMMMM");
            }
        }
    }
}
