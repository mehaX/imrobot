﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Linq;
using System.Net;
using System.Reflection;
using AutoMapper;
using FluentValidation.AspNetCore;
using ImRobot.Api.Accessors;
using ImRobot.Api.Bots;
using ImRobot.Api.Services;
using ImRobot.Application.Infrastructure;
using ImRobot.Application.Infrastructure.AutoMapper;
using ImRobot.Application.People.Models;
using ImRobot.Application.People.Queries.QueryHandlers;
using ImRobot.Persistence;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.BotFramework;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Bot.Configuration;
using Microsoft.Bot.Connector.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using LuisService = ImRobot.Api.Services.LuisService;

namespace ImRobot.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
            _env = env;
        }
        
        private IConfiguration Configuration { get; }

        private IHostingEnvironment _env;
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            services.AddMediatR();

            services.AddAutoMapper(new Assembly[] { typeof(AutoMapperProfile).GetTypeInfo().Assembly });

            var secretKey = Configuration.GetSection("botFileSecret")?.Value;

            // Loads .bot configuration file and adds a singleton that your Bot can access through dependency injection.
            var botConfig = BotConfiguration.Load(@"Api.bot", secretKey);
            var botServices = new BotServices(botConfig);

            services.AddDbContext<ImRobotDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton(sp => botConfig);
            services.AddSingleton(sp => botServices);
            services.AddSingleton(sp => new LuisService(Configuration, botServices));
            services.AddSingleton(sp => _env);

            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<PersonModel>());

            if (_env.IsProduction())
            {
                services.Configure<ForwardedHeadersOptions>(options =>
                {
                    options.KnownProxies.Add(IPAddress.Parse("188.166.194.221"));
                });
            }

            var service = botConfig.Services.FirstOrDefault(s => s.Type == "endpoint" && s.Name == (_env.IsProduction() ? "production" : "development"));
            if (!(service is EndpointService endpointService))
            {
                throw new InvalidOperationException("The .bot file does not contain a development endpoint.");
            }
            
            IStorage dataStore = new MemoryStorage();

            var conversationState = new ConversationState(dataStore);
            services.AddBot<MainBot>(options =>
            {
                options.CredentialProvider = new SimpleCredentialProvider(endpointService.AppId, endpointService.AppPassword);
                options.ChannelProvider = new ConfigurationChannelProvider(Configuration);

                // Catches any errors that occur during a conversation turn and logs them.
                options.OnTurnError = async (context, exception) =>
                {
                    await context.SendActivityAsync("Sorry, it looks like something went wrong.");
                };

                options.State.Add(conversationState);
            });

            services.AddSingleton(sp => new ConversationAccessors(conversationState));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsProduction())
            {
                app.UseForwardedHeaders(new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
                });

                //app.UseHttpsRedirection();
                app.UseAuthentication();
            }

            app.UseCors(builder => builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod());

            app.UseStatusCodePagesWithReExecute("/");
            app.UseDefaultFiles()
                .UseStaticFiles()
                .UseBotFramework();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
