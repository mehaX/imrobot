﻿using ImRobot.Api.Bots;
using ImRobot.Api.Dialogs.Main;
using Microsoft.Bot.Builder;

namespace ImRobot.Api.Accessors
{
    public class ConversationAccessors
    {
        public ConversationAccessors(ConversationState conversationState)
        {
            ConversationState = conversationState;
            MainStateAccessor = ConversationState.CreateProperty<MainState>(nameof(MainStateAccessor));
        }

        public ConversationState ConversationState { get; }
        
        public IStatePropertyAccessor<MainState> MainStateAccessor { get; set; }
    }
}
