namespace ImRobot.Api.Dialogs.Main
{
    public class MainState
    {
        public int? UserId { get; set; }

        public MainState(int? userId = 1)
        {
            UserId = userId;
        }
    }
}