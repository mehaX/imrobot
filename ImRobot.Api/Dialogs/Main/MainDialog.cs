using System;
using System.Threading;
using System.Threading.Tasks;
using ImRobot.Api.Accessors;
using ImRobot.Api.Managers;
using ImRobot.Api.Resources;
using ImRobot.Api.Services;
using ImRobot.Application.People.Queries;
using ImRobot.Domain.Enumerations;
using MediatR;
using Microsoft.Bot.Builder.Dialogs;

namespace ImRobot.Api.Dialogs.Main
{
    public class MainDialog : ComponentDialog
    {
        private ConversationAccessors _conversationAccessors;
        private LuisService _luisService;
        private IMediator _mediator;

        public new static string Id => "mainDialog";

        public MainDialog(ConversationAccessors conversationAccessors,
            LuisService luisService,
            IMediator mediator) : base(Id)
        {
            _conversationAccessors = conversationAccessors;
            _luisService = luisService;
            _mediator = mediator;

            AddDialog(new WaterfallDialog("mainDialogWaterfall", new WaterfallStep[]
            {
                InitializeMainStateAsync,
                ResumeAfterUserInputAsync
            }));
        }

        private async Task<DialogTurnResult> InitializeMainStateAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var mainState = await _conversationAccessors.MainStateAccessor.GetAsync(stepContext.Context, () => null);
            if (mainState == null)
            {
                mainState = (stepContext.Options as MainState) ?? new MainState();
                await _conversationAccessors.MainStateAccessor.SetAsync(stepContext.Context, mainState);
            }
            return await stepContext.NextAsync();
        }

        private async Task<DialogTurnResult> ResumeAfterUserInputAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var mainState = await _conversationAccessors.MainStateAccessor.GetAsync(stepContext.Context, () => null);

            var luisManager = new LuisManager(_luisService);
            await luisManager.RecognizeAsync(stepContext.Context, cancellationToken);

            var person = await _mediator.Send(new GetPersonQuery() { Id = mainState.UserId });

            string response;
            switch (luisManager.TopScoringIntent)
            {
                case LuisIntent.Name:
                    response = String.Format(TextResource.Name, person.Name);
                    break;

                case LuisIntent.Age:
                    response = String.Format(TextResource.Age, person.Age.ToString(), person.BirthdateMonth);
                    break;

                case LuisIntent.Job:
                    response = String.Format(TextResource.Job, person.Job);
                    break;

                case LuisIntent.Greetings:
                    response = TextResource.Greetings;
                    break;

                case LuisIntent.ChangeUser:
                    var name = luisManager.GetEntity("Person");
                    if (!String.IsNullOrEmpty(name))
                    {
                        var newPerson = await _mediator.Send(new GetPersonQuery() { Name = name });
                        if (newPerson != null)
                        {
                            mainState.UserId = newPerson.Id;
                            await _conversationAccessors.MainStateAccessor.SetAsync(stepContext.Context, mainState);

                            response = String.Format(TextResource.UpdateUser, newPerson.Name);
                            break;
                        }
                    }
                    response = String.Format(TextResource.UpdateUser404, stepContext.Context.Activity.Text);
                    break;

                default:
                    response = TextResource.NotFound;
                    break;
            }

            await stepContext.Context.SendActivityAsync(response, cancellationToken: cancellationToken);
            return await stepContext.EndDialogAsync();
        }
    }
}