using System;
using System.Threading;
using System.Threading.Tasks;
using ImRobot.Domain;
using ImRobot.Domain.Enumerations;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Extensions.Configuration;

namespace ImRobot.Api.Services
{
    public class LuisService
    {
        private LuisRecognizer _recognizer;

        public LuisService(IConfiguration configuration, BotServices botServices)
        {
            var luisApplicationName = configuration.GetSection("LuisApplicationName")?.Value;
            _recognizer = botServices.LuisServices[luisApplicationName];
        }

        public async Task<LuisIntent> RetrieveIntent(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await RetrieveResult(turnContext, cancellationToken);
            var res = result?.GetTopScoringIntent();
            
            var intent = LuisIntent.None;
            if (res != null && res.Value.score >= 0.55)
            {
                Enum.TryParse(res.Value.intent, out intent);
            }

            return intent;
        }

        public async Task<RecognizerResult> RetrieveResult(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _recognizer.RecognizeAsync(turnContext, cancellationToken);
        }
    }
}