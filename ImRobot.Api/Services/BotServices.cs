﻿using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Configuration;
using System.Collections.Generic;

namespace ImRobot.Api.Services
{
    public class BotServices
    {
        public BotServices(BotConfiguration botConfiguration)
        {
            foreach (var service in botConfiguration.Services)
            {
                switch (service.Type)
                {
                    case ServiceTypes.Luis:
                        {
                            var luis = (Microsoft.Bot.Configuration.LuisService)service;

                            var endpoint = (luis.Region?.StartsWith("https://") ?? false) ? luis.Region : luis.GetEndpoint();
                            var app = new LuisApplication(luis.AppId, luis.AuthoringKey, endpoint);
                            var recognizer = new LuisRecognizer(app);
                            LuisServices.Add(luis.Name, recognizer);
                            break;
                        }
                }
            }
        }

        public Dictionary<string, LuisRecognizer> LuisServices { get; } = new Dictionary<string, LuisRecognizer>();
    }
}
