﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ImRobot.Api.Accessors;
using ImRobot.Api.Dialogs.Main;
using ImRobot.Api.Resources;
using ImRobot.Api.Services;
using ImRobot.Application.People.Queries;
using MediatR;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;

namespace ImRobot.Api.Bots
{
    public class MainBot : IBot
    {
        private IStatePropertyAccessor<DialogState> _dialogStateAccessors;
        private ConversationAccessors _conversationAccessors;
        private readonly LuisService _luisService;
        private readonly IMediator _mediator;

        private DialogSet Dialogs { get; set; }

        public MainBot(ConversationAccessors conversationAccessors,
            LuisService luisService,
            
            IMediator mediator)
        {
            _dialogStateAccessors = conversationAccessors.ConversationState.CreateProperty<DialogState>(nameof(DialogState));
            _conversationAccessors = conversationAccessors;
            _luisService = luisService;
            _mediator = mediator;

            Dialogs = new DialogSet(_dialogStateAccessors);
            Dialogs.Add(new MainDialog(_conversationAccessors, _luisService, _mediator));
        }

        public async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var activity = turnContext.Activity;

            var dialogContext = await Dialogs.CreateContextAsync(turnContext);
            if (activity.Type == ActivityTypes.Message)
            {
                await dialogContext.BeginDialogAsync(MainDialog.Id);
            }
            else if (activity.Type == ActivityTypes.ConversationUpdate)
            {
                if (activity.MembersAdded != null)
                {
                    // Iterate over all new members added to the conversation.
                    foreach (var member in activity.MembersAdded)
                    {
                        // Greet anyone that was not the target (recipient) of this message.
                        // To learn more about Adaptive Cards, see https://aka.ms/msbot-adaptivecards for more details.
                        if (member.Id != activity.Recipient.Id)
                        {
                            await turnContext.SendActivityAsync(TextResource.Welcome, cancellationToken: cancellationToken);
                        }
                    }
                }
            }
            else if (activity.Type == ActivityTypes.Event)
            {
                if (activity.Name == "UpdateUser")
                {
                    try
                    {
                        var userId = Convert.ToInt32(activity.Value.ToString());
                        var newPerson = await _mediator.Send(new GetPersonQuery() { Id = userId });
                        if (newPerson != null)
                        {
                            var mainState = await _conversationAccessors.MainStateAccessor.GetAsync(turnContext, () => new MainState());
                            mainState.UserId = newPerson.Id;
                            await _conversationAccessors.MainStateAccessor.SetAsync(turnContext, mainState);
                        }
                    }
                    catch(Exception ex)
                    {
                    }
                }
            }

            await _conversationAccessors.ConversationState.SaveChangesAsync(turnContext);
        }
    }
}
