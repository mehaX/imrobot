﻿using ImRobot.Api.Services;
using ImRobot.Domain;
using Microsoft.Bot.Builder;
using System;
using System.Threading;
using System.Threading.Tasks;
using ImRobot.Domain.Enumerations;

namespace ImRobot.Api.Managers
{
    public class LuisManager
    {
        private LuisService _luisService;

        private RecognizerResult result;
        

        public LuisManager(LuisService luisService)
        {
            _luisService = luisService;
        }

        public async Task RecognizeAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            result = await _luisService.RetrieveResult(turnContext, cancellationToken);
        }

        public LuisIntent TopScoringIntent
        {
            get
            {
                var res = result?.GetTopScoringIntent();

                var intent = LuisIntent.None;
                if (res != null && res.Value.score >= 0.55)
                {
                    Enum.TryParse(res.Value.intent, out intent);
                }

                return intent;
            }
        }

        public string GetEntity(string name)
        {
            try
            {
                return result?.Entities[name][0].ToString();
            }
            catch(Exception ex)
            { }
            return null;
        }
    }
}
