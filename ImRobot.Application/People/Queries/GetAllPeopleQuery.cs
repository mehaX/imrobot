﻿using ImRobot.Application.People.Models;
using MediatR;
using System.Collections.Generic;

namespace ImRobot.Application.People.Queries
{
    public class GetAllPeopleQuery : IRequest<List<PersonModel>>
    {
    }
}
