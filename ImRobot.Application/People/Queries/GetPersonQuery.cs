﻿using ImRobot.Application.People.Models;
using MediatR;

namespace ImRobot.Application.People.Queries
{
    public class GetPersonQuery : IRequest<PersonModel>
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
