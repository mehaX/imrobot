﻿using ImRobot.Application.People.Models;
using ImRobot.Domain.Entities;
using ImRobot.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ImRobot.Application.People.Queries.QueryHandlers
{
    public class GetPersonQueryHandler : IRequestHandler<GetPersonQuery, PersonModel>
    {
        private ImRobotDbContext _dbContext;

        public GetPersonQueryHandler(ImRobotDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PersonModel> Handle(GetPersonQuery request, CancellationToken cancellationToken)
        {
            Person entity = null;
            if (request.Id != null)
            {
                entity = await _dbContext.People.FindAsync(request.Id);
            }
            else if (request.Name != null)
            {
                var name = request.Name.ToLower();
                entity = (await _dbContext.People.ToListAsync()).FirstOrDefault(p => p.Name.ToLower().Contains(name));
            }

            if (entity == null)
            {
                return null;
            }

            return PersonModel.Create(entity);
        }
    }
}
