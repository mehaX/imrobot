﻿using ImRobot.Application.People.Models;
using ImRobot.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ImRobot.Application.People.Queries.QueryHandlers
{
    public class GetAllPeopleQueryHandler : IRequestHandler<GetAllPeopleQuery, List<PersonModel>>
    {
        private ImRobotDbContext _dbContext;

        public GetAllPeopleQueryHandler(ImRobotDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<PersonModel>> Handle(GetAllPeopleQuery request, CancellationToken cancellationToken)
        {
            return _dbContext.People.Select(PersonModel.Projection).ToListAsync();
        }
    }
}
