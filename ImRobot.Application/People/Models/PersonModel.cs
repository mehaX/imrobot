﻿using ImRobot.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace ImRobot.Application.People.Models
{
    public class PersonModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Birthdate { get; set; }
        public string Job { get; set; }

        public int Age
        {
            get
            {
                return new DateTime(DateTime.Now.Subtract(Birthdate).Ticks).Year - 1;
            }
        }

        public string BirthdateMonth
        {
            get
            {
                return Birthdate.ToString("MMMMM");
            }
        }

        public static Expression<Func<Person, PersonModel>> Projection
        {
            get
            {
                return person => new PersonModel
                {
                    Id = person.Id,
                    Name = person.Name,
                    Birthdate = person.Birthdate,
                    Job = person.Job
                };
            }
        }

        public static PersonModel Create(Person person)
        {
            return Projection.Compile().Invoke(person);
        }
    }
}
